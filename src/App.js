import './App.css';
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import Main from "./components/main/Main";

function Page() {
  return (
    <div className="Page">
      <Header />
      <Main />
      <Footer />
    </div>
  );
}

export default Page;
