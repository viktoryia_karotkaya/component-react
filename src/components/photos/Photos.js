import PropTypes from "prop-types";
import "./Photos.css";

function Photos(props){
  const size = 10;
  return (
    <>
      {props.data.slice(0, size).map((photo) => (
        <div className="Photo" key={photo.id}>
          <img src={photo.url} alt="" height= {600} />
          <h2 className="photo-title" >{photo.title}</h2>
        </div>
      ) )}
    </>
  );
}

Photos.propTypes = {
  photo: PropTypes.object
};

export default Photos;
