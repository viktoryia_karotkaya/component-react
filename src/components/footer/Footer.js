import PropTypes from "prop-types";
import "./Footer.css";

function Footer(props ){
  return (
    <footer className="Footer">
      <h3>{props.title || 'Footer title'}</h3>
    </footer>
  );
}

Footer.propTypes = {
  title: PropTypes.string
};
export default Footer;
