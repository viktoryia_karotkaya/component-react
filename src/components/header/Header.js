import PropTypes from "prop-types";
import "./Header.css"
function  Header({title = 'Header title'}){
  return (
    <header className="Header">
      <h1>{title}</h1>
    </header>
  );
}
Header.propTypes = {
  title: PropTypes.string
};
export default Header;
