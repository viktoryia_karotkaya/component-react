import {data} from "../../data";
import Photos from "../photos/Photos";
import "./Main.css"

function Main(){
  return (
    <main className="Main">
      <Photos data={data} />
    </main>
    
  );
}

export default Main;
